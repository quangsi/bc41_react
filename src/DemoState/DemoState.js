import React, { Component } from 'react';

export default class DemoState extends Component {
  //   state : quản lý các giá trị ảnh hưởng tới việc update layout
  state = {
    username: 'alice',
  };
  handleChangeUsername = () => {
    // this.setState() : sinh ra để update giá trị của state
    let name;
    if (this.state.username == 'bob') {
      name = 'alice';
    } else {
      name = 'bob';
    }
    this.setState({
      username: name,
    });
  };
  //   conditon ? value1  : value2
  render() {
    return (
      <div>
        <h2>DemoState</h2>
        <h3
          className={
            this.state.username == 'bob' ? 'text-danger' : 'text-primary'
          }
        >
          {this.state.username}
        </h3>
        <button onClick={this.handleChangeUsername} className="btn btn-warning">
          Change username
        </button>
      </div>
    );
  }
}

import React, { Component } from 'react';

export default class ConditinalRendering extends Component {
  isLogin = false;
  handleDangNhap = () => {
    console.log('before', this.isLogin);
    this.isLogin = true;
    console.log('after', this.isLogin);
  };
  handleDangXuat = () => {
    console.log('before', this.isLogin);
    this.isLogin = false;
    console.log('after', this.isLogin);
  };

  renderContentButton = () => {
    if (this.isLogin) {
      // TH đã đăng nhập
      return (
        <button onClick={this.handleDangXuat} className="btn btn-danger">
          Đăng xuất
        </button>
      );
    } else {
      return (
        <button onClick={this.handleDangNhap} className="btn btn-success">
          Đăng nhập
        </button>
      );
    }
  };
  render() {
    return (
      <div>
        <h2>ConditinalRendering</h2>
        <div>{this.renderContentButton()}</div>
      </div>
    );
  }
}

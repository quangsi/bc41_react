import React, { Component } from 'react';
import CartShoe from './CartShoe';
import { data_shoe } from './data_shoe';
import ListShoe from './ListShoe';

export default class Ex_Shoe_Shop extends Component {
  state = {
    listShoe: data_shoe,
    cart: [],
  };
  handleChangeQuantity = (idShoe, luaChon) => {
    // luaChon : 1 hoặc -1
    let cloneCart = [...this.state.cart];

    let index = cloneCart.findIndex((item) => {
      return item.id == idShoe;
    });
    cloneCart[index].soLuong = cloneCart[index].soLuong + luaChon;
    this.setState({
      cart: cloneCart,
    });
  };
  handleAddToCart = (shoe) => {
    // th1 :chưa có trong giỏi hàng => push
    // th2 : đã có trong giỏ hàng => update key soLuong
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((item) => {
      return item.id == shoe.id;
    });
    if (index == -1) {
      // clone  và update
      let newShoe = { ...shoe, soLuong: 1 };
      console.log(`  🚀: Ex_Shoe_Shop -> handleAddToCart -> newShoe`, newShoe);
      console.log(`  🚀: Ex_Shoe_Shop -> handleAddToCart -> shoe`, shoe);
      cloneCart.push(newShoe);
    } else {
      cloneCart[index].soLuong++;
    }
    this.setState({
      cart: cloneCart,
    });
  };
  handleDelete = (idShoe) => {
    // thay vì xoá bằng splice() , mình sẽ tạo mảng mới từ mảng cũ mà phần tử có id khác với idShoe
    let newCart = this.state.cart.filter((item) => {
      return item.id != idShoe;
    });
    this.setState({ cart: newCart });
  };
  render() {
    return (
      <div className="container">
        <h2>Ex_Shoe_Shop</h2>
        <div className="row">
          <div className="col-8">
            {this.state.cart.length > 0 && (
              <CartShoe
                handleChangeQuantity={this.handleChangeQuantity}
                handleDelete={this.handleDelete}
                cart={this.state.cart}
              />
            )}
          </div>
          <div className="col-4">
            <ListShoe
              handleAdd={this.handleAddToCart}
              list={this.state.listShoe}
            />
          </div>
        </div>
      </div>
    );
  }
}
// props , state, renderWithMap

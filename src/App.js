import "./App.css";
import Ex_Shoe_Shop_Redux from "./Ex_Shoe_Shop_Redux/Ex_Shoe_Shop_Redux";

function App() {
  return (
    <div className="App">
      {/* --- buổi 1 --- */}
      {/* <DemoClass /> */}
      {/* <DemoClass /> */}
      {/* <DemoFunction></DemoFunction> */}
      {/* <DemoFunction /> */}
      {/* <Ex_Layout /> */}
      {/* <DataBinding /> */}

      {/* --- buổi 2 --- */}
      {/* <EventHandling /> */}
      {/* <ConditinalRendering /> */}
      {/* <DemoState /> */}
      {/* <RenderWithMap /> */}
      {/* <Ex_Car_Color /> */}

      {/* --- buổi 3 --- */}

      {/* <DemoProps /> */}
      {/* <Ex_Shoe_Shop /> */}
      {/* <Ex_Phone/> */}

      {/*  ---  Redux --- */}
      {/* <Ex_Demo_Redux_Mini /> */}
      <Ex_Shoe_Shop_Redux />
    </div>
  );
}

export default App;

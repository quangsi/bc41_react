import React, { Component } from "react";
import { connect } from "react-redux";

class ItemShoe extends Component {
  render() {
    console.log("props", this.props);
    let { image, name, price } = this.props.shoe;
    return (
      <div className="card col-4">
        <img src={image} className="card-img-top" alt="..." />
        <div className="card-body">
          <h5 className="card-title">{name}</h5>
          <p className="card-text">{price}</p>
          <a
            onClick={() => {
              this.props.handlePushToCart(this.props.shoe);
            }}
            href="#"
            className="btn btn-primary"
          >
            Add
          </a>
        </div>
      </div>
    );
  }
}
let mapDispatchToProps = (dispatch) => {
  // dispatch : function
  return {
    handlePushToCart: (shoe) => {
      let action = {
        type: "ADD_TO_CART",
        payload: shoe,
      };
      dispatch(action);
    },
  };
};

export default connect(null, mapDispatchToProps)(ItemShoe);
// 1

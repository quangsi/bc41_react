2 thư viện : redux , react-redux 
npm i redux react-redux




* State : của redux , nằm ngoài component 
* Reducer : Nơi xử lý các logic làm thay đổi giá trị của state
* Dispatch : gửi thông tin lên reducer để xử lý ( function )

*Action : object, mang  thông tin được gửi lên reducer, gồm 2 key
 + key type: Mô tả chức năng reducer sẽ xử lý ( bắt buộc )
 + key payload : params gửi kèm nếu có ( ví dụ: idShoe, shoe,..)

*CombineReducer() => tổng hợp các reducers thành rootReducer
*CreateStore => tạo ra store => store đưa cho Provider

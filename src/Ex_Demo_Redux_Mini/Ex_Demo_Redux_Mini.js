import React, { Component } from "react";
import { connect } from "react-redux";

class Ex_Demo_Redux_Mini extends Component {
  render() {
    console.log("props", this.props);
    return (
      <div>
        <button
          onClick={() => {
            this.props.handleGiamSoLuong(10);
          }}
          className="btn btn-danger"
        >
          -
        </button>
        <strong className="mx-5">{this.props.soLuong}</strong>
        <button
          onClick={this.props.handleTangSoLuong}
          className="btn btn-success"
        >
          +
        </button>
      </div>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    soLuong: state.numberReducer.soLuong,
  };
};
let mapDispatchToProps = (dispatch) => {
  return {
    handleTangSoLuong: () => {
      let action = {
        type: "TANG_SO_LUONG",
      };
      dispatch(action);
    },
    handleGiamSoLuong: (number) => {
      dispatch({
        type: "GIAM_SO_LUONG",
        payload: number,
      });
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Ex_Demo_Redux_Mini);

//  key : tên props (của component)

// value: giá trị muốn lấy từ state ( của redux )

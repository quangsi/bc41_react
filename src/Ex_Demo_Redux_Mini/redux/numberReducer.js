let initialValue = {
  soLuong: 100,
};
// initialValue giá trị mặc định của state khi load trang lần đầu

export const numberReducer = (state = initialValue, action) => {
  switch (action.type) {
    case "TANG_SO_LUONG": {
      console.log("xử lý tăng số lượng");
      state.soLuong++;
      return { ...state };
    }
    case "GIAM_SO_LUONG": {
      state.soLuong = state.soLuong - action.payload;
      return { ...state };
    }
    default:
      return state;
  }
};

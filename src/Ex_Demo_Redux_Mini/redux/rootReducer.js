import { combineReducers } from "redux";
import { numberReducer } from "./numberReducer";

export const rootReducer_Ex_Demo_Redux_Mini = combineReducers({
  numberReducer: numberReducer,
});

// key: quản lý reducer

// value: tên của reducer mà mình tạo ra

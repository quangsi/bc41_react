import React, { Component } from 'react';
import UserInfo from './UserInfo';

export default class DemoProps extends Component {
  state = {
    username: 'alice',
  };
  handleChagneUsername = () => {
    this.setState({
      username: 'bob',
    });
  };

  render() {
    return (
      <div>
        <h2>DemoProps</h2>
        <UserInfo
          hoTen={this.state.username}
          tuoi="20"
          hanleOnclick={this.handleChagneUsername}
        />
      </div>
    );
  }
}
// component cha
// component con
